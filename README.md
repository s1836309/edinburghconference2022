# EdinburghConference2022

## About
Repository of resources for the Edinburgh Physics PhD student highland conference 2022. Here you will find slides/instructions for each of the sessions as well as a section of examples/notebooks to run.


## Requirements
All attendees will need their own laptop with the following software installed:

For Supercomputing you require a ssh client:
- on Mac/Linux then Terminal is fine,
- on Windows, you can install ubuntu for windows from the windows store
- on Windows you can also install MobaXterm which provides an SSH client, inbuilt text file editor and X11 graphpics viewer plus a bash shell envioronment.  There is a 'portable' version of MobaXterm which does not need admin install privilages.
- on Windows, if you are not using MobaXterm, you can use putty from https://www.putty.org/ & xming X11 graphics viewer

Please check before the session that you can connect to the school of physics server `staff.ph.ed.ac.uk`

(<a href="https://www.wiki.ed.ac.uk/display/PandAIntranet/School+SSH+Gateways"> Instructions from the sopa wiki on remote computing </a>)

For Git 
- Git on local machine: https://github.com/git-guides/install-git
    - on Mac/Linux use Terminal bash shell
    - on Windows, make sure the Git Bash app is installed too, you will be doing the workshop on here
- A UoE GitLab account: https://git.ecdf.ed.ac.uk/



## Who is running what?
1. Introduction to Git - Simone G
1. Introduction to Machine Learning/Advanced Machine Learning - Harry Rendell
1. Introduction to Supercomputing - Rory Claydon && Conor Elrick
