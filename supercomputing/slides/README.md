# slides

## Building
* ```make``` - compile all sections and create main.pdf
* ```make clean``` - remove all compiled files

## Citations
Material used in these slides has been collected from various sources including
* <a href="https://github.com/EPCCed/archer2-MPI-2020-05-14"> Archer - Message Passing Programming with MPI </a>
* <a href="https://github.com/vcodreanu/SURFsara-PTC-Python-Parallel-and-GPU-Programming"> SURF - Python Parallel and GPU Programming </a>
