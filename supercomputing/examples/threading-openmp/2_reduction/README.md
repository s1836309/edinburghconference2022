# reduction examples

## Description
In this section we're going to learn more about parallel for loops, shared
variables, and one of the most common cases encapsulating these concepts:
reduction

We go through two examples of this:
1. ```manual_reduction.c``` contains code where we demonstrate how to implement
a reduction manually
1. ```easier_reduction.c``` where we use openMP to do the hard work for us!

## Building
* To compile both scripts, run ```make all```
* Set the number of threads with ```export OMP_NUM_THREADS=<N>```
* Execute with ```./manual_reduction``` for the manual implementation
* Execute with ```./easier_reduction``` for the openMP implementation

## Compilation Requirements
* Working C compiler with openMP installed
* Tested working on cplab
* Can use docker gcc:latest
