/*
  Example implementing reduction manually
  We will sum integers 1...N and check we get the right number!
  Author: Rory Claydon
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>

double stupidExensiveFunction(const long ii)
{
  double val=0.0;
  for ( int jj=0; jj<100; ++jj )
    val+=pow(val+jj,0.134)/cosh(ii/log(ii+sqrt(ii)));
  return sinh(val);
}

double checkSum( const long N )
{
  double sum=0; /* This variable may be updated by each thread! */
  for ( long ii=0; ii<N; ++ii )
  {
    sum+=stupidExensiveFunction(ii+1);
  }
  return sum;
}

double simpleReduction( const long N )
{
  double sum=0;
  #pragma omp parallel for reduction(+: sum) default(none) shared(N)
  for ( long ii=0; ii<N; ++ii )
  {
    sum+=stupidExensiveFunction(ii+1);
  }
  return sum;
}

int main()
{

  /* initialise timing variables */
  double start;
  double end;

  /* sum random big number */
  const long N { 10000 };

  start = omp_get_wtime();
  double total=simpleReduction(N);
  end = omp_get_wtime();
  double ptime = end - start;
  printf("Parallel work took %f seconds\n", ptime);

  start = omp_get_wtime();
  double check = checkSum(N);
  end = omp_get_wtime();
  double stime = end - start;
  printf("Serial work took %f seconds\n", stime);

  printf("---------------------------------------------------------------\n");
  printf("Speed up: %f %% \n", stime/ptime*100);

  if ( (total-check)==0 )
  {
    printf("Test passed!\n");
    return 0;
  }
  else
  {
    printf("Test failed...\n");
    printf("Expected %f but obtained %f\n",total,check);
    return 1;
  }
}
