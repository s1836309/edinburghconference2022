# helloworld example

## Description
Basic openMP example where each thread says "hello from thread <threadno>"

## Building
* To compile, run ```make helloworld```
* Set the number of threads with ```export OMP_NUM_THREADS=<N>```
* Execute with ```./helloworld```




## Compilation Requirements
* Working C compiler with openMP installed
* Tested working on cplab
* Can use docker gcc:latest 
