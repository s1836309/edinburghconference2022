#include <stdio.h>
#include <mpi.h>

int main(int argc, char** argv){
    int process_Rank, size_Of_Comm;
    int data;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size_Of_Comm);
    MPI_Comm_rank(MPI_COMM_WORLD, &process_Rank);
    
    int mydata = process_Rank;
    
    if(process_Rank == 0)
    {
        int buffer[size_Of_Comm];
        MPI_Gather(&mydata, 1, MPI_INT, buffer, 1, MPI_INT, 0, MPI_COMM_WORLD);
        printf("Values collected on process %d: ", process_Rank);
        for (int it=0;it<size_Of_Comm;it++){
			printf("%d ", buffer[it]);
		}
		printf("\n");
    }
    else
    {
        MPI_Gather(&mydata, 1, MPI_INT, NULL, 0, MPI_INT, 0, MPI_COMM_WORLD);
    }
    
	MPI_Finalize();
	return 0;
}
