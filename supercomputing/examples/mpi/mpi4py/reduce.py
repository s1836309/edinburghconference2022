"""
Basic reduce with numpy arrays

Adapted from courses publically available from SURF www.surf.nl

Author: CE
"""

from mpi4py import MPI
import numpy as np

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

#send rank multiple times as a np array
sendbuf = np.zeros(3, dtype='i') + rank
recvbuf = None

if rank !=0:
  print("[%d] Sending array:" % (rank), sendbuf)
  
if rank == 0:
  recvbuf = np.zeros(3, dtype='i')
  
#Sum arrays together
comm.Reduce([sendbuf, MPI.INT], [recvbuf, MPI.INT], op=MPI.SUM, root=0)

if rank == 0:
  sum = sum(range(size))
  assert (recvbuf[:]==sum).all()
  print(recvbuf)
