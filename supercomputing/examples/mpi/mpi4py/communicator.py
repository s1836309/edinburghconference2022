"""
Basic identification

Adapted from courses publically available from SURF www.surf.nl

Author: CE
"""
from mpi4py import MPI
comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()
print('Number of processes is %d.' %size)
print('Hello, I am process %d.' % rank)
