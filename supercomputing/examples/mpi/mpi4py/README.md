# mpi4py examples

## Description
Collection of mpi4py examples illustrating basic usage. Uses numpy arrays as a standard use-case in physics.

## Building
* Load MPI libraries if not done so already

## Execution
* ```mpirun -n <numberofprocs> python <.py file>```

## Compilation Requirements
* Tested working on cplab

### Scripts
* ```communicator.py``` - Prints rank information.
* ```p2psend.py``` - Example of blocking communication using np arrays. Sends an array of 1000 integers from one rank to another.
* ```p2pisend.py``` - Example of non-blocking communication using np arrays. Sends an array of 1000 integers from one rank to another.
* ```bcast.py``` - Broadcasts an array of 100 integers from rank 0 to all other ranks.
* ```gather.py``` - Sends an array of the proc's rank to rank 0 which gathers data into one array.
* ```scatter.py``` - Scatters an array of integers between other ranks.
* ```reduce.py``` - Collects an array from other ranks and sums the result.



