# helloworld example

## Description
Basic MPI example where each proccess says "I am rank <rank> out of <total>"

## Building
* Load MPI libraries if not done so already
* To compile, run ```make helloworld```
* Execute with ```mpirun -n <numberofprocs> helloworld```



## Compilation Requirements
* Working mpicc compiler
* Tested working on cplab
