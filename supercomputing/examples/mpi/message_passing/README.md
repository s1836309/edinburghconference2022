# basic message-passing examples

## Description
Basic MPI example where one process sends information (an integer) to another

## Building
* Load MPI libraries if not done so already
* To compile, run ```make```
* Execute with ```mpirun -n 2 p2psend```

## Blocking vs Non-blocking
In this directory there are 2 programmes to send information from one process to another. One of which uses blocked communication ```p2psend``` and one uses unblocked communication ```p2pisend```. 

Running p2pisend illustrates what can and will happen if a barrier is needed that is not there.

## Compilation Requirements
* Working mpicc compiler
* Tested working on cplab
