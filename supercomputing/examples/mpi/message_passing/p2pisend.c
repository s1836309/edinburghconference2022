/*
 * Non-blocking version of p2psend.c to illustrate when things go wrong
 */
 
#include <stdio.h>
#include <mpi.h>

int main(int argc, char** argv) {
    int process_Rank, size_Of_Cluster, message_Item;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size_Of_Cluster);
    MPI_Comm_rank(MPI_COMM_WORLD, &process_Rank);
    MPI_Request request = MPI_REQUEST_NULL;
    MPI_Status status;

    if(process_Rank == 0){
        message_Item = 42;
       
        MPI_Isend(&message_Item, 1, MPI_INT, 1, 1, MPI_COMM_WORLD, &request);
        printf("Message Sent: %d\n", message_Item);
    }

    else if(process_Rank == 1){
        MPI_Irecv(&message_Item, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, &request);
        printf("Message Received: %d\n", message_Item);
    }
    
    MPI_Wait(&request, &status);

    MPI_Finalize();
    return 0;
}
